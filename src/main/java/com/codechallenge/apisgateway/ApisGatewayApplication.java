package com.codechallenge.apisgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * This class defines implements the authentication layers
 * Kindly ignore for now
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
public class ApisGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApisGatewayApplication.class, args);
	}

}
